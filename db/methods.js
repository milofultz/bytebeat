const { User } = require('./models.js');

const createNewUser = userData => {
  return new Promise((resolve, reject) => {
    // create password hash using bcrypt, eventually
    const newUser = new User({
      username: userData.username,
      email: userData.email,
      password: userData.password, // TODO: Make bcrypt hash
      profileContent: '', // always empty on creation
    });

    newUser.save((errors) => {
      if (!errors) {
        console.log('New user created: ', newUser);
        resolve(newUser);
      } else {
        reject('An error has occurred.\n', errors);
      }
    });

  })
};

module.exports = {
  createNewUser,
};


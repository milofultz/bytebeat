const bcrypt = require('bcrypt');
const mongoose = require('mongoose');
const { SALT_WORK_FACTOR } = require('../constants.js');

const userSchema = new mongoose.Schema({
  // ID is automatically added
  username: {
    required: true,
    type: String,
    unique: true,
  },
  email: {
    required: true,
    type: String,
  },
  // Avatar eventually? Gravatar may be better, or smth else
  password: {
    required: true,
    type: String,
  },
  profileContent: String,
});

userSchema.pre('save', function (next) {
  const user = this;

  if (!user.isModified('password')) {
    return next();
  }

  // use bcrypt to encode password and replace plaintext with hahsed
  bcrypt.genSalt(SALT_WORK_FACTOR)
    .then(salt => bcrypt.hash(user.password, salt))
    .then(hash => {
      user.password = hash;
      return next();
    })
    .catch(err => next(err));
});

userSchema.methods.comparePassword = function(candidatePassword) {
  return bcrypt.compare(candidatePassword, this.password);
};

const postSchema = new mongoose.Schema({
  // Foreign key of user ID
  user: {
    type: mongoose.Schema.Types.ObjectId,
    ref: "User",
  },
  content: {
    required: true,
    type: String,
  },
  timestamp: {
    required: true,
    type: Date,
  },
  pinned: Boolean,
});

const User = mongoose.model('User', userSchema);
const Post = mongoose.model('Post', postSchema);

module.exports = {
  User,
  Post,
};


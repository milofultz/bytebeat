/**
 * Create a WAV file as blob from user-supplied bytebeat formula.
 * Modified from original: https://mathr.co.uk/misc/2021-12-23_bytebeat.html
 */
const makeBytebeat = function (equation) {
  // Set length of bytebeat in Bytes
  // * Must have 0 <= l <= 0xFF, and l0 + 0x24 <= 0xFF
  // * Currently set at a 1:00 audio blob
  const l0  = 0;
  const l8  = 0x50;
  const l16 = 0x07;
  const l24 = 0;
  const n = l0 + (l8 << 8) + (l16 << 16) + (l24 << 24);

  // Create new WAV file and add RIFF header
  const WAV = new Uint8Array(44 + n);
  WAV.set(new Uint8Array(
    // Annotate the rest of this for your own convenience
    [
      // R  I     F     F     Length in bytes
      0x52, 0x49, 0x46, 0x46, 0x24+l0, l8,   l16,  l24,
      0x57, 0x41, 0x56, 0x45, 0x66,    0x6d, 0x74, 0x20,
      0x10, 0x00, 0x00, 0x00, 0x01,    0x00, 0x01, 0x00,
      0x40, 0x1f, 0x00, 0x00, 0x40,    0x1f, 0x00, 0x00,
      0x01, 0x00, 0x08, 0x00, 0x64,    0x61, 0x74, 0x61,
      l0,   l8,   l16,  l24
    ]
  ));

  // Write bytebeat audio to WAV file
  for (let t = 0; t < n; ++t) {
    const step = equation(t);
    WAV[44 + t] = step & 0xFF;
  }

  const blob = new Blob([WAV], {type: "audio/wav"});
  return blob;
};


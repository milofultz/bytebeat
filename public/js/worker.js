importScripts('./bytebeat.js', './parser.js');

onmessage = function (e) {
  const equation = e.data[0];
  const id = e.data[1];

  const compiledEquation = compile(equation);
  const blob = makeBytebeat(compiledEquation);
  const bytebeatAudio = URL.createObjectURL(blob);
  postMessage([bytebeatAudio, id]);
}

// Import and initiate the database
const { mongoose } = require('./db/db.js');
const { User, Post } = require('./db/models.js');
const { createNewUser } = require('./db/methods.js');

// Compile pug files for cached rendering
const pug = require('pug');
const renderHome = pug.compileFile('./pug/home.pug');
const renderLogin = pug.compileFile('./pug/login.pug');
const renderRegister = pug.compileFile('./pug/register.pug');
const renderPost = pug.compileFile('./pug/post.pug');
const renderPosts = pug.compileFile('./pug/posts.pug');

// Import server architecture
const express = require('express');
const session = require('express-session');
const MongoStore = require('connect-mongo');
const { body, validationResult } = require('express-validator');

// Initiate server and add middleware
const app = express();
const PORT = 4000;
app.use(express.json());
app.use(express.static('public'));

// Create session storage for express-session
const sessionStore = MongoStore.create({
  client: mongoose.connection.getClient(),
  dbName: 'test',
  collectionName: 'sessions',
  stringify: false,
  // Only refresh session once a day maximum
  touchAfter: 24 * 3600,
});

app.use(session({
  secret: 'REPLACE-WITH-ENV-IN-PRODUCTION',
  store: sessionStore,
  cookie: {
    maxAge: 30 * 24 * 60 * 60 * 1000,
    // Use in production only. Doesn't work locally
    // secure: true,
  },
  resave: false,
  saveUninitialized: false,
}));

// Make test data
const makeTestData = async function() {
  try {
    await User.deleteMany();
    await Post.deleteMany();

    const bob = {
      username: 'bob',
      email: 'bob@bob.com',
      password: 'stuff',
      profileContent: 'profile stuff',
    };

    const userBob = await createNewUser(bob);

    const bobsPost1 = new Post({
      user: userBob._id, // I assume this will eventually be a find call or maybe pulling from the token or something
      content: 't * ((t >> 12 | t >> 8) & 63 & t >> 4)',
      timestamp: new Date(),
      pinned: false,
    });

    const bobsPost2 = new Post({
      user: userBob._id,
      content: 't%(t>>8|t>>16)',
      timestamp: new Date(),
      pinned: true,
    });

    await bobsPost1.save();
    await bobsPost2.save();
  } catch (err) {
    console.error(err);
  }
};

makeTestData();

// Create routes
app.get('/login', (req, res) => {
  const page = renderLogin();
  res.end(page);
});

app.post('/login', async (req, res) => {
  const { username, password } = req.body;

  // If user with given username doesn't exist, reject
  const user = await User.findOne({ username }).exec();
  if (!user) {
    return res.status(401).send();
  }

  try {
    // Verify the JSON credentials are accurate using bcrypt
    const isMatch = await user.comparePassword(password);
    if (!isMatch) {
      // If password doesn't match, reject
      return res.status(401).send();
    }

    // Regenerate session token to guard against sesion fixation
    req.session.regenerate(function(err) {
      if (err) {
        return res.status(500).send();
      }

      // Store username in the session
      req.session.user = username;

      // Save the session before redirection so page load happens post-save
      req.session.save(function(err) {
        if (err) {
          return res.status(500).send();
        }

        // Send OK after successful login
        res.status(200).send();
      });
    });
  } catch(err) {
    res.status(500).send('An error has occurred');
  }
});

app.post('/logout', async (req, res) => {
  // get user from session
  const user = req.session.user;
  if (user) {
    res.send({ user });
  } else {
    res.send({ body: 'No user!' });
  }
});

app.get('/post', async (req, res) => {
  const thisPost = await Post.findOne()
    .populate('user')
    .exec();
  const page = renderPost({ post: thisPost });
  res.end(page);
});

app.post(
  '/user/create',
  body('username').isLength({ min: 3, max: 20 }),
  body('email').isEmail(),
  body('password').isLength({ min: 12 }),
  async (req, res) => {
    try {
      const errors = validationResult(req);

      if (!errors.isEmpty()) {
        // send back a 400 with list of errors, PHP style
        res.status(400).send(errors);
      } else {
        const formData = req.body;
        await createNewUser(formData);
        res.status(201).send();
      }
    } catch (err) {
      res.status(500).send(err);
    }
  }
);

app.get('/user', async (req, res) => {
  const userPosts = await Post.find().populate('user').lean();
  const page = renderPosts({ posts: userPosts });
  res.end(page);
});

app.get('/register', async (req, res) => {
  const page = renderRegister();
  res.end(page);
});

app.get('/', async (req, res) => {
  const page = renderHome();
  res.end(page);
});

app.listen(PORT, () => console.log(`Listening on http://localhost:${PORT}`));


# bytebeat online player

* outputs audio capture if you want?
* Has interactive help re: waveforms
* Like dwitter but for bytebeats
* User auth
* public/private
* CRUD for your own bytebeats

* Do I need to make a parser for user input of bytebeat equations, considering they will be executed? Is there another way to take user input and have it executed as an actual equation?
  * eval works, but obviously has problems of remote code execution. Is there a way around this?
  * I think it may be easy enough to filter out unwanted stuff. List of accepted math symbols (e.g. `[0-9]()+-*/<>&|^`), list of accepted Math method calls (e.g. `sin`, `cos`, `tan`), and then list of accepted variables (I think `t` would be it?). So it would just be a regex call I think, at the end of the day. lol I think it would end up being a parser, using BNF or whatever. Or use mathjs (https://mathjs.org/index.html)
  * Making a parser was better than mathjs or expr-eval. And especially for this project, it is very much good enough.

https://github.com/greggman/html5bytebeat/blob/master/html5bytebeat.html

---

Bytebeat/WAV info:

* https://docs.fileformat.com/audio/wav/ (used this to make the output of the HTML/JS audio element a mono file instead of a stereo file)

---

Basic idea is kind of a dwitter type page but for bytebeats. Use Javascript to render the audio out to an `<audio>` element that can then be played. You have an account, a limit to how much text to play, can view each user's stuff, blah blah.

# Plan

## Storage

MongoDB because why not. Gotta relearn how to do pretty much anything with it, but will be a good project.

### Schema

* Users
  * PK ID:int
  * avatar(? Don't know if I want to deal with file storage):text/url
  * username:text
  * profileDescription:text
  * email:text
  * password hash:binary
* Posts
  * PK ID:int
  * FK user (ID) ?? How do I do a foreign key in Mongoose/MongoDB? Put it into tinybrain
  * content:text
  * timestamp:datetime
  * pinned:boolean
* Followers (join table of user to user)
  * PK ID:int
  * FK user (ID)
  * FK follower (user ID)

## View

Could use React just to minimize the variables. And for the sake of job searches or whatever. But I think I would rather use something I align with more. A simpler framework or something else like Svelte?

Or better, just do it all with vanilla JS. It would take a bit more planning, but I would be much much more proud of the result. And I HATE dealing with all the tooling and bullshit. Like max, I'd use jQuery, but even that seems unnecessary unless I get a *really* good reason for it.

It looks like Svelte compiles down to vanilla JS. Could be a cool thing to do at the very end, once it's done?

### Pages

* Always
  * User (User's posts)
  * Post (a single post)
* Not Logged In
  * Home (Need to register or log in, blurb)
  * Register
  * Log In
* Logged In
  * Home (Posts from everybody you follow in time order)
  * Log out (just a server query, but still)
  * New post (form)
  * Preview post (render new audio element before submission) (Could be just debounced on the new post page after 1 second of changes)

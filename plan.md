NEXT:

* ~~Make the post a partial~~
* ~~Make user page, injecting posts as a partial~~
* ~~Create schemas for User and Post~~
* ~~Add some default values to the database~~

* ~~Create a basic API route for (can't without net for express)~~
  * ~~a single post~~
  * ~~all of the posts of a single user~~

* ~~Separate the DB instantiation from the main server file~~
  * ~~Move models to own file~~
  * ~~Move rest of DB out so DB can connect in other file and only export what is necessary~~

* ~~Set up pug to be server-side rendering~~
  * ~~Will need to do a DB query and then pass the results in to the Pug render~~
  * ~~Need the Pug package to do this~~
  * ~~Set up pug to render all user posts the same way as a single post~~

* ~~Figure out how to make eval safe for evaluating user input equations~~
  * ~~Tried mathjs, but compilation and evaluation was way too slow for how many times it needed to run for a whole file (8s vs just 500ms with eval)~~
  * ~~I can sanitize the input with regex I am pretty positive, to a certain degree of resilience~~
  * ~~Ensure the output is a number **always**. If it ever comes out to a NaN, then it should throw~~
  * ~~[This MDN article](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/eval#never_use_eval!) mentions that using `eval` is bad, but a lot of problems can be mitigated with an alternative. So that will help.~~
  * ~~None of this worked, bleh. Gotta find a better solution.~~
  * ~~https://github.com/hotfooted/bytebeat~~
  * ~~jesus, is this really that complicated? It may be most useful to make a parser situation. At least it's good practice!~~

* ~~Now it is a blocking process, need to put it in the background.~~
  * ~~https://blog.logrocket.com/how-javascript-works-optimizing-for-parsing-efficiency/~~
  * ~~Web Workers would be good~~
  * ~~Implement lazy loading with intersection observer https://developer.mozilla.org/en-US/docs/Web/API/Intersection_Observer_API~~

* ~~Refactor everything into logical spaces~~
  * ~~Javascript in the pug file mainly~~
  * ~~Utilities to own file~~

* ~~Register page~~
  * ~~Create dummy register pug page~~
  * ~~Create mongo command that allows a new user to be created~~
  * ~~Connect to arbitrary button on register page so click makes a new user~~
  * ~~Create form on front end to help users register~~
  * ~~Test form will submit using dummy code~~
  * ~~Create route to accept new POST data from form and log it to server~~
  * ~~Connect new user creation to form submission~~
  * ~~Validate front end input with HTML5 validation using JS~~
  * ~~Style form~~
  * ~~Validate back end input within method (try catch, await)~~
    * ~~https://express-validator.github.io/docs/~~
  * ~~Redirect to homepage if valid~~
  * ~~Stay on register page if back end does not validate properly, display error message~~

* ~~Login Page~~
  * ~~Extract makeInput mixin from register to file~~
  * ~~Create dummy login page~~
  * ~~Implement bcrypt for registration and login~~
    * https://scribe.rip/@justinmanalad/pre-save-hooks-in-mongoose-js-cf1c0959dba2
    * https://scribe.rip/front-end-weekly/how-to-create-a-simple-authorization-login-using-bcrypt-react-and-ajax-d71ed919f5cb
  * ~~Create mongo command that allows user to log in and sends back a session token~~
    * ~~Create a mongo table for session tokens for every user? Or is a simple JWT with expiration sufficient? (https://redis.com/blog/json-web-tokens-jwt-are-dangerous-for-user-sessions/ for example)~~
  * ~~Research session tokens etc. for Jaavascript and Node with Mongo~~
  * ~~Implement session token system for login/post-registration~~

* Logout button
  * ~~Determine if there is a way to logout without a server call using cookies~~
  * ~~Ensure secure flag is set on cookie to avoid problems https://security.stackexchange.com/questions/140326/delete-cookie-or-set-httponly-and-secure~~
  * Read more about Token security https://cheatsheetseries.owasp.org/cheatsheets/Session_Management_Cheat_Sheet.html
  * Logout removes token cookie and deletes token from table
  * Sends user back to home
  * Remove old tokens via expiry date automatically (nightly maintenance?)

* How will sessions be handled for logged in users? See rails setup
  * https://gomakethings.com/api-authentication-with-vanilla-js/
  * https://gomakethings.com/where-should-you-store-authentication-tokens/
  * Use `express-session` for auto handling
    * https://github.com/expressjs/session#readme
    * https://developer.okta.com/blog/2021/06/07/session-mgmt-node
  * cblgh cerca: session mgmt in go https://github.com/cblgh/cerca/blob/main/server/session/session.go
  * https://stackoverflow.com/questions/68841518/storing-sessions-with-express-session-connect-mongo-and-mongoose
  * https://stackoverflow.com/questions/11826792/node-express-session-expiration

* MISC
  * Break up `app.js` into smaller files
    * Express imports for different routes/setups (login, logout, registration, posts, etc.)
    * Pug broken up into those files as well for pulling in layouts or whatever.
